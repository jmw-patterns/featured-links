# Dudley Patterns Framework: Featured Links Module
This package is designed for use with the Dudley plugin. You
can include it in your project by requiring this repository in the 
Dudley plugin's `composer.json` file.

In the `require` object of `composer.json`, add this line:
```
"dudley\featured-links": "dev-master"
```

## How to Use in Template Files
Like all modules in the Dudley Patterns Framework, action names are generated from
both the object's `$meta_type` property and its `$action_name` property.
The format is `dudley_{$meta_type}_{$action_name}`.

Thus, if you are using Advanced Custom Fields, the code to include in your
template file to render the featured links would be:
`<?php do_action( 'dudley_acf_featured_links' ); ?>`

## Default Location
Front Page

## Available Filters
* `dudley_featured_links_item_image_size`: Alters the size of the featured links item image
(default is 'medium'). Return an empty string to get the full-sized image.

## How to Override the Markup in Your Own Template File
1. If none exists, create a directory in your theme named `dudley-modules`
2. Add a file named `featured-links.php` in the `dudley-modules` directory.
