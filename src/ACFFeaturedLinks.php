<?php
namespace Dudley\Patterns\Pattern\FeaturedLinks;

/**
 * Class ACFFeaturedLinks
 *
 * @package Dudley\Patterns\Pattern\FeaturedLinks
 */
class ACFFeaturedLinks extends FeaturedLinks {
	/**
	 * @var string
	 */
	public static $meta_type = 'acf';

	/**
	 * ACFFeaturedLinks constructor.
	 */
	public function __construct() {
		if ( ! get_field( 'featured_links' ) ) {
			return;
		}

		while ( has_sub_field( 'featured_links' ) ) {
			$this->add_item( new FeaturedLinksItem(
				get_sub_field( 'featured_links_item_heading' ),
				get_sub_field( 'featured_links_item_link' ),
				get_sub_field( 'featured_links_item_image' )
			) );
		}

		parent::__construct( $this->items );
	}
}
