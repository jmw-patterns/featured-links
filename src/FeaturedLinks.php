<?php
namespace Dudley\Patterns\Pattern\FeaturedLinks;

use Dudley\Patterns\Abstracts\AbstractRepeater;

/**
 * Class FeaturedLinks
 *
 * @package Dudley\Patterns\Pattern\FeaturedLinks
 */
class FeaturedLinks extends AbstractRepeater {
	/**
	 * Base action name for this module.
	 *
	 * @var string
	 */
	public static $action_name = 'featured_links';

	/**
	 * FeaturedLinks constructor.
	 */
	public function __construct( array $items ) {
		$this->items = $items;
	}

	/**
	 * Requirements to render this module.
	 *
	 * @return array
	 */
	public function requirements() {
		return [
			$this->items,
		];
	}
}
