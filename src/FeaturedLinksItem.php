<?php
namespace Dudley\Patterns\Pattern\FeaturedLinks;

use Dudley\Patterns\Abstracts\AbstractItem;
use Dudley\Patterns\Traits\HeadingTrait;
use Dudley\Patterns\Traits\ImageTrait;

/**
 * Class FeaturedLinksItem
 *
 * @package Dudley\Patterns\Pattern\FeaturedLinks
 */
class FeaturedLinksItem extends AbstractItem {
	use HeadingTrait;
	use ImageTrait;

	/**
	 * Post object to link to.
	 *
	 * @var \WP_Post
	 */
	private $link;

	/**
	 * FeaturedLinksItem constructor.
	 *
	 * @param $heading
	 * @param $link
	 * @param $image
	 */
	public function __construct( $heading, $link, $image ) {
		$this->heading  = $heading;
		$this->link     = $link;
		$this->img      = $image;
		$this->img_size = $this->set_img_size( 'medium', FeaturedLinks::$action_name );
	}

	/**
	 * Requirements to render this object.
	 *
	 * @return array
	 */
	public function requirements() {
		return [
			$this->heading,
			$this->img,
			$this->link,
		];
	}

	/**
	 * Print the link's URL.
	 */
	public function url() {
		echo esc_url( get_permalink( $this->link->ID ) );
	}
}
