<?php
/**
 * @var $module Dudley\Patterns\Pattern\FeaturedLinks\FeaturedLinks
 * @var $item   Dudley\Patterns\Pattern\FeaturedLinks\FeaturedLinksItem
 */
?>

<section class="featured-links">
	<div class="featured-links__inner">
		<ul class="featured-links__list">
			<?php foreach ( $module->get_items() as $item ) : ?>
				<li class="featured-links__item featured-link">
					<a class="featured-link__link"
					   href="<?php $item->url(); ?>" style="background-image: url(<?php $item->img_url(); ?>">
						<h4 class="featured-link__hd"><?php $item->heading(); ?></h4>
					</a>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
</section><!-- .featured-links -->
